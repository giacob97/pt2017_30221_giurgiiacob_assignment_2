package model;

public class Task {
	private int arrivalTime;
	private int processingTime;
	private int finishTime;
	private int waitingTime;
	private int id;
	
	
	public Task(){
		
	}
	
	public Task(int arrivalTime, int processingTime,int id) {
		this.arrivalTime = arrivalTime;
		this.processingTime = processingTime;
		this.id = id;
	}
	
	
	
	public int getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(int waitingTime) {
		this.waitingTime = waitingTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getProcessingTime() {
		return processingTime;
	}
	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}
	public int getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}
}
