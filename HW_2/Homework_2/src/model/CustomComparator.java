package model;

import java.util.Comparator;

public class CustomComparator implements Comparator<Task>{

	@Override
	public int compare(Task a, Task b) {
		return new Integer(a.getArrivalTime()).compareTo(b.getArrivalTime());
	}

}
