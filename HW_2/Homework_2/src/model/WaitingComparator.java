package model;

import java.util.Comparator;

public class WaitingComparator implements Comparator<Server>{

	@Override
	public int compare(Server a, Server b) {
		return new Integer(a.getWaitingPeriod().intValue()).compareTo(b.getWaitingPeriod().intValue());
	}

}
