package model;

import java.util.Collections;
import java.util.List;


public class ConcreteStrategyTime implements Strategy{

	@Override
	public void addTask(List<Server> servers, Task t) throws InterruptedException {
	     
		Collections.sort(servers, new WaitingComparator());
		 servers.get(0).addTask(t);
			
		
	}
}
