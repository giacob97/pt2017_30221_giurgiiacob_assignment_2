package View;

import java.awt.EventQueue;

import javax.swing.JFrame;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import java.sql.ResultSet;

import javax.swing.JOptionPane;


import javax.swing.JLabel;
import javax.swing.JTextField;

import Connection.SQLConnection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;


public class Menu {

	JFrame frmBineAtiVenit;
    Connection connection = null ; 
    private JTextField textFieldUN;
    private JPasswordField passwordField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frmBineAtiVenit.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
		connection=(Connection) SQLConnection.DBConnection();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBineAtiVenit = new JFrame();
		frmBineAtiVenit.getContentPane().setBackground(Color.WHITE);
		frmBineAtiVenit.setTitle("BINE ATI VENIT!");
		frmBineAtiVenit.setBounds(100, 100, 494, 425);
		frmBineAtiVenit.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBineAtiVenit.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Username");
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 20));
		lblNewLabel.setBounds(91, 112, 96, 14);
		frmBineAtiVenit.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setFont(new Font("Calibri", Font.BOLD, 20));
		lblNewLabel_1.setBounds(91, 151, 96, 17);
		frmBineAtiVenit.getContentPane().add(lblNewLabel_1);
		
		textFieldUN = new JTextField();
		textFieldUN.setBounds(197, 110, 86, 20);
		frmBineAtiVenit.getContentPane().add(textFieldUN);
		textFieldUN.setColumns(10);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				
		}
		});
		btnLogin.setBounds(306, 112, 96, 45);
		frmBineAtiVenit.getContentPane().add(btnLogin);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(197, 150, 86, 20);
		frmBineAtiVenit.getContentPane().add(passwordField);
		
		JLabel lblNewLabel_2 = new JLabel("");

		
		frmBineAtiVenit.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		
		
		
		JButton btnNewButton = new JButton("Enter as guest\r\n");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					try{ 
				    }catch(Exception f){JOptionPane.showMessageDialog(null,f);}
				
			}
		});
		btnNewButton.setBounds(122, 203, 235, 23);
		frmBineAtiVenit.getContentPane().add(btnNewButton);
	}
}