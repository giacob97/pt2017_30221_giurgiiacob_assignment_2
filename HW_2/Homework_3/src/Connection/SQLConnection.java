package Connection;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class SQLConnection {
	Connection con = null;
	public static Connection DBConnection(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/depozit","root","root");
			JOptionPane.showMessageDialog(null,"S-a conectat cu succes la baza de date");
		    return con;
		    
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
			return null;
		}
	}
}
